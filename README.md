![addinglist.png](https://bitbucket.org/repo/Xx5E8y/images/4153100242-addinglist.png)![addingnewlist.png](https://bitbucket.org/repo/Xx5E8y/images/4021642835-addingnewlist.png)![list.jpg](https://bitbucket.org/repo/Xx5E8y/images/3484290433-list.jpg)![clickfun.jpg](https://bitbucket.org/repo/Xx5E8y/images/757966421-clickfun.jpg)![composemail.png](https://bitbucket.org/repo/Xx5E8y/images/1052249957-composemail.png)![addclient.png](https://bitbucket.org/repo/Xx5E8y/images/3322476001-addclient.png)![returnhompage.jpg](https://bitbucket.org/repo/Xx5E8y/images/2689318912-returnhompage.jpg)![homepage.jpg](https://bitbucket.org/repo/Xx5E8y/images/135101208-homepage.jpg)# Project Running Manual #

This manual shows you how to deploy this project to see the output.

### Required Software : ###

1) JDK (Version 1.8)
2) Internet Connection(For database and mailgun api)
3) Eclipse for JEE
4) Apache Tomcat 8.0.24 or above

### Steps for project setup : ###

1. Download the project from the repository.
2. install JDK
3. Open Eclipse
4. Do right click in project explorer area of the eclipse .
5. go to import -> import (An Import box will be open).
6. select "Existing project into workspace" from the list shown in the box.
7. click on browse and browse to the project directory ,select it and click ok and then click       finish.
8. Setup the server in the eclipse.
9. after setting up the project in eclipse. Open from package explorer 
   : Java Resources -> src/main/webapp ->WEB-INF -> lib
   and select all the libraries and right click on it . Go to build path and select "set to       build path".
10. after the right click on the project and go to "Run as" and select "Run on server".
11. Project will execute.

### Steps for sending a mail ###
To send a mail using the project you must have to "subscribe to receive mails from mailgun".
-> MailGun Email ID :- tariqueshamim@outlook.com
-> MailGun Password :- TarixRaxz769

### To view the database ###
Go to www.db4free.net  
Use this user ID :- raxzlucifer
Use this password :- TarixRaxz