<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href='<c:url value="/resources/css/materializes.min.css" />' rel="stylesheet" >
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<!-- Some Custom CSS -->
<style type="text/css">
	.boxSize{
			height: 58% !important;
			width: 45% !important;
	}
</style>
</head>

<body>

<!--Import jQuery before materialize.js-->
<script src="https://code.jquery.com/jquery-2.1.1.min.js" ></script>
<script src="<c:url value="/resources/js/materialize.min.js" />"></script>
<script src="<c:url value="/resources/js/custom.js" />"></script>

<!--Import some JQuery  -->
<script type="text/javascript">
	$(document).ready(function(){
    	// the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
    	$('.modal-trigger').leanModal();
  	});
	
	$(document).ready(function() {
	    $('select').material_select();
	  });
</script>

<nav>
   	<div class="nav-wrapper">
   		<a href='<c:url value="home" />' class="brand-logo">Welcome To MicroCRM [ Home Page ]</a>
   		<ul id="nav-mobile" class="right hide-on-med-and-down">
   			<li><a class="waves-effect waves-light modal-trigger tooltipped" data-position="bottom" data-delay="50" data-tooltip="Add New Client List" href="#addNewClientList"><i class="large material-icons">view_module</i></a></li>
   			<li><a class="waves-effect waves-light modal-trigger tooltipped" data-position="bottom" data-delay="50" data-tooltip="Add New Client Data" href="#addNewClientData"><i class="small material-icons">add</i></a></li>
       </ul>
   	</div>
</nav>

<div class="container">

    <!-- Page Content goes here -->
    <tiles:insertAttribute name="content" />
        
</div>

</body>
</html>