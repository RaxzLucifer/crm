package org.crm.service;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.crm.dao.ClientDataDao;
import org.crm.model.ClientData;
import org.crm.model.ClientType;
import org.crm.model.ComposeMail;
import org.springframework.beans.factory.annotation.Autowired;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class ClientDataServiceImpl implements ClientDataService{

	@Autowired
	ClientDataDao clientDataDao;
	
	@Override
	public boolean addClient(ClientData clientData) {
		return clientDataDao.addClient(clientData);
	}

	@Override
	public List<ClientData> getlList(String type) {
		return clientDataDao.getList(type);
	}

	@Override
	public ClientResponse sendMail(List<ClientData> list,String senderName,String subject,String message) {
		Client client = Client.create();
	    client.addFilter(new HTTPBasicAuthFilter("api","key-bfb0a1ae87ec6cafa01cafa0eafe3819"));
	    WebResource webResource = client.resource("https://api.mailgun.net/v3/sandbox96c7de23ae0d4c52862d3f76cfe376db.mailgun.org"
	    											+"/messages");
	    MultivaluedMapImpl formData = new MultivaluedMapImpl();
	    formData.add("from", senderName+" mailgun@sandbox96c7de23ae0d4c52862d3f76cfe376db.mailgun.org");
	    for(ClientData data : list){
	    	formData.add("to", data.getEmail());
	    }
	    //formData.add("to", "tariqueshamim@outlook.com");
	    /*formData.add("to", "YOU@YOUR_DOMAIN_NAME");*/
	    formData.add("subject", subject);
	    formData.add("text", message);
	    return webResource.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, formData);
	}

	@Override
	public List<ClientType> getTypeList() {
		return clientDataDao.getTypeList();
	}

	@Override
	public boolean addClientType(ClientType clientType) {
		return clientDataDao.addClientType(clientType);
	}

	@Override
	public void saveMails(ComposeMail composeMail) {
		clientDataDao.saveMails(composeMail);
	}
}
