package org.crm.service;

import java.util.List;

import org.crm.model.ClientData;
import org.crm.model.ClientType;
import org.crm.model.ComposeMail;

import com.sun.jersey.api.client.ClientResponse;

public interface ClientDataService {
	public boolean addClient(ClientData clientData);
	public boolean addClientType(ClientType clientType);
	public List<ClientType> getTypeList();
	public List<ClientData> getlList(String type);
	public void saveMails(ComposeMail composeMail);
	public ClientResponse sendMail(List<ClientData> list,String senderName,String subject,String message);
}
