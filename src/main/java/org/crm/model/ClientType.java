package org.crm.model;

import java.io.Serializable;

public class ClientType implements Serializable{
	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
