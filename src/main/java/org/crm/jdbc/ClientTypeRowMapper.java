package org.crm.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.crm.model.ClientType;
import org.springframework.jdbc.core.RowMapper;

public class ClientTypeRowMapper implements RowMapper<ClientType>{

	@Override
	public ClientType mapRow(ResultSet rs, int arg1) throws SQLException {
		ClientTypeExtracter clientTypeExtracter = new ClientTypeExtracter();
		return clientTypeExtracter.extractData(rs);
	}

}
