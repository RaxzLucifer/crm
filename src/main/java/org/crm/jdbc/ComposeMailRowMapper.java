package org.crm.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.crm.model.ComposeMail;
import org.springframework.jdbc.core.RowMapper;

public class ComposeMailRowMapper implements RowMapper<ComposeMail>{

	@Override
	public ComposeMail mapRow(ResultSet rs, int arg1) throws SQLException {
		ComposeMailExtracter extracter = new ComposeMailExtracter();
		return extracter.extractData(rs);
	}

}
