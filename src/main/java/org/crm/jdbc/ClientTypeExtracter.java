package org.crm.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.crm.model.ClientType;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

public class ClientTypeExtracter implements ResultSetExtractor<ClientType>{

	@Override
	public ClientType extractData(ResultSet rs) throws SQLException,DataAccessException {
		ClientType clientType = new ClientType();
		clientType.setType(rs.getString(2));
		return clientType;
	}

}
