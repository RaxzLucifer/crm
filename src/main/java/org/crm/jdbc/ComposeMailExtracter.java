package org.crm.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.crm.model.ComposeMail;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

public class ComposeMailExtracter implements ResultSetExtractor<ComposeMail>{

	@Override
	public ComposeMail extractData(ResultSet rs) throws SQLException,DataAccessException {
		ComposeMail composeMail = new ComposeMail();
		composeMail.setType(rs.getString(2));
		composeMail.setName(rs.getString(3));
		composeMail.setSubject(rs.getString(4));
		composeMail.setMessage(rs.getString(5));
		return composeMail;
	}

}
