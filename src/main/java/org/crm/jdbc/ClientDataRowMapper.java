package org.crm.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.crm.model.ClientData;
import org.springframework.jdbc.core.RowMapper;

public class ClientDataRowMapper implements RowMapper<ClientData>{

	@Override
	public ClientData mapRow(ResultSet rs, int arg1) throws SQLException {
		ClientDataExtracter clientDataExtracter = new ClientDataExtracter();
		return clientDataExtracter.extractData(rs);
	}

}
