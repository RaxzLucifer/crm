package org.crm.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.crm.model.ClientData;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

public class ClientDataExtracter implements ResultSetExtractor<ClientData>{

	@Override
	public ClientData extractData(ResultSet rs) throws SQLException,DataAccessException {
		ClientData clientData = new ClientData();
		
		clientData.setName(rs.getString(2));
		clientData.setEmail(rs.getString(3));
		clientData.setType(rs.getString(4));
		
		return clientData;
	}

}
