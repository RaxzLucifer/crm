package org.crm.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.ProcessBuilder.Redirect;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.crm.model.ClientData;
import org.crm.model.ClientType;
import org.crm.model.ComposeMail;
import org.crm.service.ClientDataService;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.*;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;


/*This is a controller to handle the request from the client*/
@Controller
public class SiteController {
	
	@Autowired
	ClientDataService clientDataService;
	
	ModelAndView view;
	static String message = "Welcome to Home Page";
	
	/*This method is used to render the homepage*/
	@RequestMapping("home")
	public ModelAndView home(HttpSession httpSession){
		List<ClientType> clientTypes = new ArrayList<ClientType>();
		clientTypes = clientDataService.getTypeList();
		httpSession.setAttribute("message", message);
		System.out.println(message);
		httpSession.setAttribute("clientTypes",clientTypes);
		view = new ModelAndView("home");
		message = "";
		return view;
	}
	
	/*This method is used to Fetch List from data base*/
	@RequestMapping("fetchList")
	public String fetchList(@RequestParam String param,HttpSession httpSession){
		List<ClientData> list = new ArrayList<ClientData>();
		list =  clientDataService.getlList(param);
		httpSession.setAttribute("list",list);
		httpSession.setAttribute("type", param);
		return "viewlist";
	}
	
	/*This method is used to Compose mail and save it to database*/
	@RequestMapping("mailAJAX")
	public String sendMail(@RequestParam String type,String senderName,String subject,String message,HttpSession session) throws IOException{
			List<ClientData> list = new ArrayList<ClientData>();
			list = clientDataService.getlList(type);
			ComposeMail mail = new ComposeMail();
			mail.setType(type);
			mail.setName(senderName);
			mail.setSubject(subject);
			mail.setMessage(message);
			clientDataService.saveMails(mail);
			System.out.println(clientDataService.sendMail(clientDataService.getlList(type),senderName,subject,message));
			SiteController.message = "Mail sended successfully";
			return "redirect";
	}
	
	/*This method is used to add client to databse in a particular list*/
	@RequestMapping(value="addClient",method=RequestMethod.POST)
	public String addClient(@ModelAttribute ClientData clientData) throws IOException{
		clientDataService.addClient(clientData);
		message = "Client : "+clientData.getName()+" Added To "+clientData.getType()+" successfully";
		return "redirect";
	}
	
	/*This method is used to create particular type of list*/
	@RequestMapping(value="addClientType",method=RequestMethod.POST)
	public String addClientList(@ModelAttribute ClientType clientType) throws IOException{
		clientDataService.addClientType(clientType);
		message = "Client List : " +clientType.getType()+ " Added successfully";
		return "redirect";
	}
}