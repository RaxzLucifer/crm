package org.crm.dao;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.crm.jdbc.ClientDataExtracter;
import org.crm.jdbc.ClientDataRowMapper;
import org.crm.jdbc.ClientTypeRowMapper;
import org.crm.model.ClientData;
import org.crm.model.ClientType;
import org.crm.model.ComposeMail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

public class ClientDataDaoImpl implements ClientDataDao{
	
	@Autowired
	DataSource dataSource;

	@Override
	public boolean addClient(ClientData clientData) {		
		Object objects[] = new Object[]{0,clientData.getName(),clientData.getEmail(),clientData.getType()};
		String sql = "insert into clientdata (id,name,email,type) values (?,?,?,?)";
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.update(sql,objects);
		System.out.println("Data Inserted Successfully");
		return true;
	}
	

	@Override
	public boolean addClientType(ClientType clientType) {
		Object object[] = new Object[]{0,clientType.getType()};
		String sql = "insert into clienttype (id,type) values(?,?)";
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.update(sql,object);
		System.out.println("Data Inserted Successfully");
		return true;
	}
	
	@Override
	public List<ClientData> getList(String type) {
		List<ClientData> homeList = new ArrayList<ClientData>();
		String sql = "select * from clientdata where type = '"+type+"' ";
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		homeList = jdbcTemplate.query(sql, new ClientDataRowMapper());
		return homeList;
	}

	@Override
	public List<ClientType> getTypeList() {
		List<ClientType> typeList = new ArrayList<ClientType>();
		String sql = "select * from clienttype";
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		typeList = jdbcTemplate.query(sql,new ClientTypeRowMapper());
		return typeList;
	}


	@Override
	public void saveMails(ComposeMail composeMail) {
		Object[] obj = new Object[]{0,composeMail.getType(),composeMail.getName(),composeMail.getSubject(),composeMail.getMessage()};
		String sql = "insert into composemail (id,type,name,subject,message) values(?,?,?,?,?)";
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.update(sql,obj);
		System.out.println("Mail Saved");
	}
}
