package org.crm.dao;

import java.util.List;

import org.crm.model.ClientData;
import org.crm.model.ClientType;
import org.crm.model.ComposeMail;

public interface ClientDataDao {
	public boolean addClient(ClientData clientData);
	public List<ClientType> getTypeList();
	public boolean addClientType(ClientType clientType);
	public List<ClientData> getList(String type);
	public void saveMails(ComposeMail composeMail);
}
