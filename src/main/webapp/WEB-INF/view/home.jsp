<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>MicroCRM</title>
</head>
<body>

	<div class="container">
        <script type="text/javascript">
        	callToast('${message}');
        </script>
        <!-- Page Content goes here -->

		<!-- Modal Structure For Adding New Client List -->
  		<div id="addNewClientList" class="boxSize modal modal-fixed-footer">
    		<div class="modal-content">
      			<h4>ADD NEW CLIENT LIST</h4>
      			
      			<div class="row">
    				<form class="col s12" action="addClientType" method="post" modelAttribute="clientType">
      					<div class="row">
      					
        					<div class="input-field col s5">
          						<i class="material-icons prefix">info</i>
          						<input id="type" type="text" class="validate" name="type" required>
          						<label>List Name</label>
        					</div>
        			
        					<div class="input-field col s6">
          						<button class="btn waves-effect waves-light" type="submit" name="action">Submit
    								<i class="material-icons">send</i>
  								</button>
        					</div>
  							
      					</div>
    				</form>
  				</div>      			
    		</div>
    	</div>

		<!-- Modal Structure For Add New Client-->
  		<div id="addNewClientData" class="boxSize modal modal-fixed-footer">
    		<div class="modal-content">
      			<h4>ADD NEW CLIENT DATA</h4>
      			
      			<div class="row">
    				<form class="col s12" action="addClient" method="post" modelAttribute="clientData">
      					<div class="row">
      					
        					<div class="input-field col s5">
          						<i class="material-icons prefix">account_circle</i>
          						<input id="name" type="text" class="validate" name="name" required>
          						<label for="icon_prefix">Name</label>
        					</div>
        			
        					<div class="input-field col s5">
          						<i class="material-icons prefix">email</i>
          						<input id="email" type="email" class="validate" name="email" required>
          						<label for="email" data-error="wrong" data-success="right">Email</label>
        					</div>
        					
        					<div class="input-field col s5">
        						<i class="material-icons prefix">info</i>
        						<label>Select The Type of E-Mail : </label>
    						</div>
        					
        					<div class="col s5">
        						<select class="browser-default" name="type" id="type" required>
      								<option value="" disabled selected>Choose your option</option>
      								<c:forEach var="clientTypes" items="${clientTypes }">
      									<option value="${clientTypes.type }">${clientTypes.type }</option>
      								</c:forEach>
      							</select>
    						</div>
  							
  							<div class="input-field col s6">
          						<button class="btn waves-effect waves-light" type="submit" name="action">Submit
    								<i class="material-icons">send</i>
  								</button>
        					</div>
  							
      					</div>
    				</form>
  				</div>      			
    		</div>
    	</div>
        
        <!-- Model box for sending mail -->
        
  		<div id="composeBox" class="boxSize modal">
    		<div class="modal-content">
      			<div class="row">
    				<form class="col s12" method="post">
      					<div class="row">
      					
      						<div class="input-field col s12">
          						<i class="material-icons prefix">account_circle</i>
          						<input id="senderName" type="text" class="validate" required>
          						<label for="icon_prefix">Sender Name</label>
        					</div>
      						
      						<div class="input-field col s12">
          						<i class="material-icons prefix">subject</i>
          						<input id="subject" type="text" class="validate" required>
          						<label for="icon_prefix">Subject</label>
        					</div>
        						      					
        					<div class="input-field col s12">
          						<i class="material-icons prefix">mode_edit</i>
          						<textarea id="message" class="materialize-textarea" required></textarea>
          						<label for="textarea1">Message</label>
        					</div>
        					
        					<div class="input-field col s6">
          						<button class="btn waves-effect waves-light" type="submit" onclick="mailAJAX()">Send
    								<i class="material-icons">send</i>
  								</button>
        					</div>
    					</div>
      				</form>
  				</div>
    		</div>
  		</div>
		
        <div class="row">
        <!-- Group Of Mails(Clients Types) -->
        <c:forEach var="clientTypes" items="${clientTypes }">
        	<div class="col s6">
          		<div class="card blue-grey darken-1">
            		<div class="card-content white-text">
				        <span class="card-title">Mails of ${clientTypes.type }</span>
            		</div>
            		<div class="card-action">
            			<button class="btn waves-effect waves-light modal-trigger" href="#composeBox" onclick=setType("${clientTypes.type }")>Send Mail
    						<i class="material-icons">mail</i>
  						</button>
              			<a  href='<c:url value="fetchList">
              				<c:param name="param" value="${clientTypes.type}"/>
              				</c:url>'>
              				View List
              				<i class="material-icons">visibility</i>
              			</a>
            		</div>
          		</div>
        	</div>
        </c:forEach>
        </div>
	</div>        
</body>
</html>