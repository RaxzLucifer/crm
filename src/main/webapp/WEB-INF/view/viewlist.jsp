<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List of ${type }</title>
</head>
<body>

	<h3 align="center">List Of Available Client In ${type } List</h3>
	<table class="hoverable">
       		<thead>
          		<tr>
              		<th data-field="id">Name</th>
              		<th data-field="name">Email</th>
              		<th data-field="price">Type</th>
          		</tr>
        	</thead>
        	
        	<c:choose>
        		<c:when test="${not empty list}">
        			<tbody>
        			<c:forEach var="list" items="${list}">
          				<tr>
            				<td>${list.name }</td>
            				<td>${list.email }</td>
            				<td>${list.type }</td>
          				</tr>
          			</c:forEach>
        			</tbody>
        		</c:when>
        		<c:otherwise>
        			<h6>No Client Data available in ${type } List!!!</h6>
        		</c:otherwise>
        	</c:choose>
      </table>
</body>
</html>