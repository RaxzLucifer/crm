var xmlHTTP = getXMLObject();
var type = "";
var subject = "";
var message = "";
var senderName = "";

function getXMLObject()  
{
   var xmlHttp = false;
   try {
     xmlHttp = new ActiveXObject("Msxml2.XMLHTTP")
   }
   catch (e) {
     try {
       xmlHttp = new ActiveXObject("Microsoft.XMLHTTP")
     }
     catch (e2) {
       xmlHttp = false
     }
   }
   if (!xmlHttp && typeof XMLHttpRequest != 'undefined') {
     xmlHttp = new XMLHttpRequest(); 
   }
   return xmlHttp;
}

function setType(value){
	type = value;
}
function getType() {
	return type;
}

/*function for composing mail*/
function mailAJAX(){
	subject = document.getElementById("subject").value;
	message = document.getElementById("message").value;
	senderName = document.getElementById("senderName").value;
	if(xmlHTTP && subject && message && senderName && type){
		xmlHTTP.open("GET","mailAJAX?type="+getType()+"&senderName="+senderName+"&subject="+subject+"&message="+message, false);
		xmlHTTP.send(null);
	}
}

function callToast(message) {
	Materialize.toast(message, 3000);
}

